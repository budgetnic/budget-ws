# Budget Rent a Car - Mail Service

## Installation
Heroku CLI and Maven are needed to run the project. Using Heroku CLI 6.14.34.

### Setup
To compile and package the application into a WAR
``` Setup mvn clean package ```

## Running tests
```shell
$ mvn test
```

### Running project locally
```shell
$ mvn clean package jetty:run
```

## Deploying to Heroku

### Login with your account and select the Heroku project

```shell
$ heroku login
$ heroku git:remote -a budget-nic-ws-staging  
```

### Add the files to your repo and commit them

```shell
$ git add .
$ git commit -m "message" 
```

### Push to Heroku

```shell
$ git push -f heroku <current_branch>:master
```

