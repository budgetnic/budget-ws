package com.pernix.budget.factories;

import com.pernix.budget.pojos.RevisionRequest;
import com.pernix.budget.pojos.Vehicle;
import com.pernix.budget.ws.utils.EnglishReportTranslation;

import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;

import java.io.StringWriter;

public class HTMLFactory {

    public static String generateHTML(RevisionRequest revisionRequest, String file, Vehicle vehicle){
        VelocityEngine velocityEngine = new VelocityEngine();
        velocityEngine.setProperty("resource.loader", "class");
        velocityEngine.setProperty("class.resource.loader.class",
                "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");

        velocityEngine.init();
        Template template = velocityEngine.getTemplate(file);
        VelocityContext context = new VelocityContext();
        context.put("revisionRequest", revisionRequest);
        context.put("vehicle", vehicle);
        context.put("OptionTranslator", EnglishReportTranslation.class);
        context.put("OptionTranslators", EnglishReportTranslation.class);
        context.put("Option", EnglishReportTranslation.class);
        context.put("Options", EnglishReportTranslation.class);
        StringWriter writer = new StringWriter();
        template.merge(context, writer);
        return writer.toString();
    }
}
