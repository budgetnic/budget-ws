package com.pernix.budget.mailer;

import com.pernix.budget.pojos.Revision;
import com.pernix.budget.pojos.Vehicle;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;

public class MailService {

    private static Properties mailServerProperties;
    private static Session getMailSession;
    private static Revision revision;
    private static Vehicle vehicle;

    MailService(Session session, Properties properties) {
        session.setDebug(true);
        MailService.mailServerProperties = properties;
        MailService.getMailSession = session;
    }

    public void generateAndSendEmail(ArrayList<String> recipients, String subject, String body,
                                     ByteArrayOutputStream[] attachmentsFiles) throws MessagingException, IOException {

        MimeMessage generateMailMessage = new MimeMessage(getMailSession);

        for(String recipient : recipients){
            generateMailMessage.addRecipient(Message.RecipientType.TO, new InternetAddress(recipient));
        }

        generateMailMessage.setSubject(subject);

        // Create the message part
        BodyPart messageBodyPart = new MimeBodyPart();
        messageBodyPart.setContent(body, "text/html; charset=utf-8");

        // Create a multipart message
        Multipart multipart = new MimeMultipart();
        multipart.addBodyPart(messageBodyPart);

        for(ByteArrayOutputStream filename : attachmentsFiles){
            BodyPart messageBodyPart2 = new MimeBodyPart();
            DataSource attachment = new ByteArrayDataSource(filename.toByteArray(),"application/pdf");
            messageBodyPart2.setDataHandler(new DataHandler(attachment));
            messageBodyPart2.setFileName("Reporte - " + revision.getContractNumber() + " - " +
                    vehicle.getKey() + " - " + revision.getVehicleMVA() + ".pdf");
            multipart.addBodyPart(messageBodyPart2);
        }
        // Send the complete message parts
        generateMailMessage.setContent(multipart);
        Transport transport = getMailSession.getTransport(mailServerProperties.getProperty("budget.email.protocol"));

        transport.connect(
                mailServerProperties.getProperty("budget.email.host"),
                mailServerProperties.getProperty("budget.email.user"),
                mailServerProperties.getProperty("budget.email.password"));
        transport.sendMessage(generateMailMessage, generateMailMessage.getAllRecipients());
        transport.close();
    }

    static void setRevision(Revision revision) {
        MailService.revision = revision;
    }

    public static void setVehicle(Vehicle vehicle) {
        MailService.vehicle = vehicle;
    }

}
