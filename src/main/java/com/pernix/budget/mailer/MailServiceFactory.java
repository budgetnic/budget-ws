package com.pernix.budget.mailer;

import com.pernix.budget.pojos.Revision;
import com.pernix.budget.pojos.Vehicle;
import com.pernix.budget.resources.ResourceManager;

import javax.mail.Session;
import java.io.IOException;
import java.util.Properties;

public class MailServiceFactory {
    private static MailService mailService = null;
    private static Properties mailServerProperties;

    public static MailService getMailService(Revision revision, Vehicle vehicle) {
        if(mailServerProperties == null)
            getMailServiceProperties();
        mailService = (mailService != null) ? mailService : (new MailService(getMailServiceSession(),
                mailServerProperties));
        MailService.setRevision(revision);
        MailService.setVehicle(vehicle);
        return mailService;
    }

    private static Session getMailServiceSession(){
        return Session.getDefaultInstance(mailServerProperties, null);
    }

    private static void getMailServiceProperties(){
        mailServerProperties = new Properties();
        try {
            mailServerProperties.load(
                    ResourceManager.getResourceAsInputStream("mailservice.properties"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
