package com.pernix.budget.pojos;

public class CarParts {

    private String antenna;
    private String spare_tire;
    private String radio;
    private String camera;
    private String tools;
    private String legal_documents;
    private String triangle;
    private String extinguisher;
    private String bearing_year;
    private String circulation;
    private String insurance;
    private String emission_gases;
    private String mechanical_inspection;
    private String rack;
    private String carpet;
    private String emblems;
    private String emergency_kit;
    private String plates;
    private String mud_flap;
    private String keey;
    private String seat_covers;
    private String side_lights;
    private String roof_molding;
    private String windscreen;
    private String doorlock;
    private String rear_view_mirror;
    private String fuel_cap;
    private String engine_caps;


    public CarParts() {
    }

    public CarParts(String antenna, String spare_tire, String radio, String camera,
                    String tools, String legal_documents, String triangle, String extinguisher,
                    String bearing_year, String circulation, String insurance, String emission_gases,
                    String mechanical_inspection, String rack, String carpet, String emblems,
                    String emergency_kit, String plates, String mud_flap, String keey, String seat_covers,
                    String side_lights, String roof_molding, String windscreen, String doorlock, 
                    String rear_view_mirror, String fuel_cap, String engine_caps) {
        this.antenna = antenna;
        this.spare_tire = spare_tire;
        this.radio = radio;
        this.camera = camera;
        this.tools = tools;
        this.legal_documents = legal_documents;
        this.triangle = triangle;
        this.extinguisher = extinguisher;
        this.bearing_year = bearing_year;
        this.circulation = circulation;
        this.insurance = insurance;
        this.emission_gases = emission_gases;
        this.mechanical_inspection = mechanical_inspection;
        this.rack = rack;
        this.carpet = carpet;
        this.emblems = emblems;
        this.emergency_kit = emergency_kit;
        this.plates = plates;
        this.mud_flap = mud_flap;
        this.keey = keey;
        this.seat_covers = seat_covers;
        this.side_lights = side_lights;
        this.roof_molding = roof_molding;
        this.windscreen = windscreen;
        this.doorlock = doorlock;
        this.rear_view_mirror = rear_view_mirror;
        this.fuel_cap = fuel_cap;
        this.engine_caps = engine_caps;
    }

    public String getAntenna() {
        return antenna;
    }

    public void setAntenna(String antenna) {
        this.antenna = antenna;
    }

    public String getSpare_tire() {
        return spare_tire;
    }

    public void setSpare_tire(String spare_tire) {
        this.spare_tire = spare_tire;
    }

    public String getRadio() {
        return radio;
    }

    public void setRadio(String radio) {
        this.radio = radio;
    }

    public String getCamera() {
        return camera;
    }

    public void setCamera(String camera) {
        this.camera = camera;
    }

    public String getTools() {
        return tools;
    }

    public void setTools(String tools) {
        this.tools = tools;
    }

    public String getLegal_documents() {
        return legal_documents;
    }

    public void setLegal_documents(String legal_documents) {
        this.legal_documents = legal_documents;
    }

    public String getTriangle() {
        return triangle;
    }

    public void setTriangle(String triangle) {
        this.triangle = triangle;
    }

    public String getExtinguisher() {
        return extinguisher;
    }

    public void setExtinguisher(String extinguisher) {
        this.extinguisher = extinguisher;
    }

    public String getBearing_year() {
        return bearing_year;
    }

    public void setBearing_year(String bearing_year) {
        this.bearing_year = bearing_year;
    }

    public String getCirculation() {
        return circulation;
    }

    public void setCirculation(String circulation) {
        this.circulation = circulation;
    }

    public String getInsurance() {
        return insurance;
    }

    public void setInsurance(String insurance) {
        this.insurance = insurance;
    }

    public String getEmission_gases() {
        return emission_gases;
    }

    public void setEmission_gases(String emission_gases) {
        this.emission_gases = emission_gases;
    }

    public String getMechanical_inspection() {
        return mechanical_inspection;
    }

    public void setMechanical_inspection(String mechanical_inspection) {
        this.mechanical_inspection = mechanical_inspection;
    }

    public String getRack() {
        return rack;
    }

    public void setRack(String rack) {
        this.rack = rack;
    }

    public String getCarpet() {
        return carpet;
    }

    public void setCarpet(String carpet) {
        this.carpet = carpet;
    }

    public String getEmblems() {
        return emblems;
    }

    public void setEmblems(String emblems) {
        this.emblems = emblems;
    }

    public String getEmergency_kit() {
        return emergency_kit;
    }

    public void setEmergency_kit(String emergency_kit) {
        this.emergency_kit = emergency_kit;
    }

    public String getPlates() {
        return plates;
    }

    public void setPlates(String plates) {
        this.plates = plates;
    }

    public String getMud_flap() {
        return mud_flap;
    }

    public void setMud_flap(String mud_flap) {
        this.mud_flap = mud_flap;
    }

    public String getKeey() {
        return keey;
    }

    public void setKeey(String keey) {
        this.keey = keey;
    }

    public String getSeat_covers() {
        return seat_covers;
    }

    public void setSeat_covers(String seat_covers) {
        this.seat_covers = seat_covers;
    }

    public String getSide_lights() {
        return side_lights;
    }

    public void setSide_lights(String side_lights) {
        this.side_lights = side_lights;
    }

    public String getRoof_molding() {
        return roof_molding;
    }

    public void setRoof_molding(String roof_molding) {
        this.roof_molding = roof_molding;
    }

    public String getWindscreen() {
        return windscreen;
    }

    public void setWindscreen(String windscreen) {
        this.windscreen = windscreen;
    }

    public String getDoorlock() {
        return doorlock;
    }

    public void setDoorlock(String doorlock) {
        this.doorlock = doorlock;
    }

    public String getRear_view_mirror() {
        return rear_view_mirror;
    }

    public void setRear_view_mirror(String rear_view_mirror) {
        this.rear_view_mirror = rear_view_mirror;
    }

    public String getFuel_cap() {
        return fuel_cap;
    }

    public void setFuel_cap(String fuel_cap) {
        this.fuel_cap = fuel_cap;
    }

    public String getEngine_caps() {
        return engine_caps;
    }

    public void setEngine_caps(String engine_caps) {
        this.engine_caps = engine_caps;
    }
}