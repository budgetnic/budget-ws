package com.pernix.budget.pojos;

public class Damage {

    private String part;
    private String damage;
    private String severity;
	private String isNew;

    public String getIsNew() {
        return isNew;
    }

    public void setIsNew(String isNew) {
        this.isNew = isNew;
    }

    public String getPart() {
        return part;
    }

    public void setPart(String part) {
        this.part = part;
    }

    public String getDamage() {
        return damage;
    }

    public void setDamage(String damage) {
        this.damage = damage;
    }
    
    public String getSeverity() {
        return severity;
	}

	public void setSeverity(String severity) {
		this.severity = severity;
	}
}
