package com.pernix.budget.pojos;

public class Revision {
  private String gasLevel;
  private String deliveryPlace;
  private String km;
  private String timestamp;
  private String type;
  private String username;
  private String vehicleMVA;
  private CarParts carParts;
  private String canvas;
  private String signature;
  private String contractNumber;
  private String drive;
  private String phone;
  private String numberMovement;
  private String delivery_place_d;
  private String delivery_place_o;
  private String checkOutTime;
  private String deliveryTimeInfo;
  private String deliveryTime;
  private boolean isTimeExceeded;
  private String date;
  private String optionType;

  public String getOptionType() {
    return optionType;  
  }

  public void setOptionType(String optionType) {
    this.optionType = optionType;
  }


  public String getDeliveryTime() {
    return deliveryTime;  
  }

  public void setDeliveryTime(String deliveryTime) {
    this.deliveryTime = deliveryTime;
  }

  public String getDate() {
    return date;  
  }

  public void setDate(String date) {
    this.date = date;
  }

  public boolean getIsTimeExceeded() {
    return isTimeExceeded;  
  }

  public void setIsTimeExceeded(boolean isTimeExceeded) {
    this.isTimeExceeded = isTimeExceeded;
  }

  public String getSignature() {
    return signature;  
  }

  public void setSignature(String signature) {
    this.signature = signature;
  }

  public String getCheckOutTime() {
    return checkOutTime;  
  }

  public void setCheckOutTime(String checkOutTime) {
    this.checkOutTime = checkOutTime;
  }

  public String getDeliveryTimeInfo() {
    return deliveryTimeInfo;  
  }

  public void setDeliveryTimeInfo(String deliveryTimeInfo) {
    this.deliveryTimeInfo = deliveryTimeInfo;
  }

  public String getCanvas() {
    return canvas;
  }

  public void setCanvas(String canvas) {
    this.canvas = canvas;
  }

  public String getGasLevel() {
    return gasLevel;
  }

  public void setGasLevel(String gasLevel) {
   this.gasLevel = gasLevel;
  }

  public String getDeliveryPlace() {
    return deliveryPlace;
  }

  public void setDeliveryPlace(String deliveryPlace) {
    this.deliveryPlace = deliveryPlace;
  }

  public void setDelivery_place_o(String delivery_place_o) {
    this.delivery_place_o = delivery_place_o;
  }

  public String getDelivery_place_o() {
    return delivery_place_o;
  }

  public void setDelivery_place_d(String delivery_place_d) {
    this.delivery_place_d = delivery_place_d;
  }

  public String getDelivery_place_d() {
    return delivery_place_d;
  }

  public String getKm() {
    return km;
  }

  public void setKm(String km) {
    this.km = km;
  }

  public String getTimestamp() {
    return timestamp;
  }

  public void setTimestamp(String timestamp) {
    this.timestamp = timestamp;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getVehicleMVA() {
    return vehicleMVA;
  }

  public void setVehicleMVA(String vehicleMVA) {
    this.vehicleMVA = vehicleMVA;
  }

  public CarParts getCarParts() {
    return carParts;
  }

  public void setCarParts(CarParts carParts) {
    this.carParts = carParts;
  }

  public String getContractNumber() {
    return contractNumber;
  }

  public void setContractNumber(String contractNumber) {
    this.contractNumber = contractNumber;
  }

  public String getDrive() {
    return drive;
  }

  public void setDrive(String drive) {
    this.drive = drive;
  }

  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }

  public String getNumberMovement() {
    return numberMovement;
  }

  public void setNumberMovement(String numberMovement) {
    this.numberMovement = numberMovement;
  }
}