package com.pernix.budget.pojos;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public class Vehicle {
  @SerializedName("MVA")
  private String mva;
  @SerializedName("PSC")
  private String psc;
  private String brand;
  private String color;
  private String gas;
  private String key;
  @SerializedName("last_revision_ref")
  private String lastRevisionRef;
  @SerializedName("license_plate")
  private String licensePlate;
  private String model;
  @SerializedName("traction_type")
  private String tractionType;
  private String type;
  private String year;

  public Vehicle() {
  }

  public Vehicle(String year, String mva, String psc, String brand, String color, String gas, String key, String lastRevisionRef, String licensePlate, String model, String tractionType, String type) {
    this.year = year;
    this.mva = mva;
    this.psc = psc;
    this.brand = brand;
    this.color = color;
    this.gas = gas;
    this.key = key;
    this.lastRevisionRef = lastRevisionRef;
    this.licensePlate = licensePlate;
    this.model = model;
    this.tractionType = tractionType;
    this.type = type;
  }

  public String getBrand() {
    return brand;
  }

  public void setBrand(String brand) {
    this.brand = brand;
  }

  public String getMva() {
    return mva;
  }

  public void setMva(String mva) {
    this.mva = mva;
  }

  public String getPsc() {
    return psc;
  }

  public void setPsc(String psc) {
    this.psc = psc;
  }

  public String getColor() {
    return color;
  }

  public void setColor(String color) {
    this.color = color;
  }

  public String getGas() {
    return gas;
  }

  public void setGas(String gas) {
    this.gas = gas;
  }

  public String getKey() {
    return key;
  }

  public void setKey(String key) {
    this.key = key;
  }

  public String getLastRevisionRef() {
    return lastRevisionRef;
  }

  public void setLastRevisionRef(String lastRevisionRef) {
    this.lastRevisionRef = lastRevisionRef;
  }

  public String getLicensePlate() {
    return licensePlate;
  }

  public void setLicensePlate(String licensePlate) {
    this.licensePlate = licensePlate;
  }

  public String getModel() {
    return model;
  }

  public void setModel(String model) {
    this.model = model;
  }

  public String getTractionType() {
    return tractionType;
  }

  public void setTractionType(String tractionType) {
    this.tractionType = tractionType;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public String getYear() {
    return year;
  }

  public void setYear(String year) {
    this.year = year;
  }
}