package com.pernix.budget.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.pernix.budget.pojos.Vehicle;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class FirebaseService {

  private static final String FIREBASE_PATH =
    "https://budget-nic-developer.firebaseio.com/";    // DEVEL
    // "https://budget-nic.firebaseio.com/";  // PRODUCTION

  private static FirebaseService firebaseService = new FirebaseService();

  private FirebaseService() {}

  public static FirebaseService getInstance() {
    return firebaseService;
  }

  public static Vehicle getVehicle(String mva) {
    String vehicleUrl = FIREBASE_PATH + "vehicles/" + mva + ".json";
    HttpURLConnection connection = null;
    Vehicle vehicle = new Vehicle();
    Gson gson = new Gson();

    try {
      URL url = new URL(vehicleUrl);
      connection = (HttpURLConnection) url.openConnection();
      connection.setRequestMethod("GET");

      int responseCode = connection.getResponseCode();

      BufferedReader in = new BufferedReader(
        new InputStreamReader(connection.getInputStream()));
      String inputLine;
      StringBuffer response = new StringBuffer();
      while ((inputLine = in.readLine()) != null) {
        response.append(inputLine);
      }
      in.close();

      vehicle = gson.fromJson(response.toString(), Vehicle.class);

    } catch (IOException e) {
      e.printStackTrace();
    }
    return vehicle;
  }

}
