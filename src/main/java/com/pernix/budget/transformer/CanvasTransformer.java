package com.pernix.budget.transformer;

import com.itextpdf.text.pdf.codec.Base64;
import com.pernix.budget.pojos.RevisionRequest;
import org.apache.batik.transcoder.TranscoderException;
import org.apache.batik.transcoder.TranscoderInput;
import org.apache.batik.transcoder.TranscoderOutput;
import org.apache.batik.transcoder.image.PNGTranscoder;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class CanvasTransformer {

  public static String transformToBase64Png(String canvas){
    String result = "";
    try {
      canvas = canvas.replace("stroke-dasharray=\"\"", "").replace("stroke-width=\"none\"","" ).replace("stroke-width=\"\"", "").replace("font-size=\"none\"", "").replace("font-weight=\"none\"", "").replace("font-family=\"none\"", "").replace("text-anchor=\"none\"", "");
      InputStream inputStream = new ByteArrayInputStream(canvas.getBytes());
      TranscoderInput inputSvgImage = new TranscoderInput(inputStream);
      ByteArrayOutputStream pngOutput = new ByteArrayOutputStream();
      TranscoderOutput outputPngImage = new TranscoderOutput(pngOutput);
      PNGTranscoder myConverter = new PNGTranscoder();
      myConverter.transcode(inputSvgImage, outputPngImage);
      pngOutput.flush();
      pngOutput.close();
      result = Base64.encodeBytes(pngOutput.toByteArray());
    } catch (TranscoderException | IOException e) {
      e.printStackTrace();
    }
    return result;
  }
  
  
  
}
