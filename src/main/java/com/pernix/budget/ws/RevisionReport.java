package com.pernix.budget.ws;

import com.itextpdf.text.DocumentException;
import com.pernix.budget.factories.HTMLFactory;
import com.pernix.budget.factories.PDFFactory;
import com.pernix.budget.mailer.MailServiceFactory;
import com.pernix.budget.pojos.Revision;
import com.pernix.budget.pojos.RevisionRequest;
import com.pernix.budget.pojos.RevisionResponse;
import com.pernix.budget.pojos.Vehicle;
import com.pernix.budget.services.FirebaseService;
import com.pernix.budget.transformer.CanvasTransformer;
import com.pernix.budget.ws.utils.WSConstants;

import javax.mail.MessagingException;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.io.*;
import java.util.ArrayList;

@Path(WSConstants.VERSION + WSConstants.REVISIONS_WS)
public class RevisionReport {

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String getDummy(){
        return "Test";
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public RevisionResponse createRevision(RevisionRequest revisionRequest) throws DocumentException {
        RevisionResponse response = new RevisionResponse();
        try {
            ArrayList<String> email = new ArrayList<>();
            email.add(revisionRequest.getEmail());
            if(!revisionRequest.getCcMail().isEmpty()){
              email.add(revisionRequest.getCcMail());
            }

            if(!revisionRequest.getDeliveryPlaceMail().isEmpty()){
              email.add(revisionRequest.getDeliveryPlaceMail());
            }
            revisionRequest.getRevision().setCanvas(CanvasTransformer.transformToBase64Png(revisionRequest.getRevision().getCanvas()));

            String languageReport = (revisionRequest.getLanguage().equals("spanish"))?"report-spanish.template.vm":"report-english.template.vm";
            String languageRevision = (revisionRequest.getLanguage().equals("spanish"))?"revision-spanish.template.vm":"revision-english.template.vm";
            String emailSubject = (revisionRequest.getLanguage().equals("spanish"))?"Grupo ABP - Su Revisión":"Grupo ABP - Your Revision";
            Vehicle vehicle = FirebaseService.getVehicle(revisionRequest.getRevision().getVehicleMVA());
            String stringReport = HTMLFactory.generateHTML(revisionRequest, languageReport, vehicle);
            ByteArrayOutputStream[] attachments = {(ByteArrayOutputStream) PDFFactory.createPDF(stringReport)};
            MailServiceFactory.getMailService(revisionRequest.getRevision(), vehicle)
                    .generateAndSendEmail(
                            email, emailSubject,
                            HTMLFactory.generateHTML(revisionRequest, languageRevision, vehicle),
                            attachments);

            response.setMessage("Revision was sent!");
            response.setStatus(WSConstants.STATUS_OK);
        } catch (MessagingException | IOException e) {
            e.printStackTrace();
            response.setStatus(WSConstants.STATUS_FAILED);
            response.setMessage("Sorry, revision was NOT sent!");
        }

        return response;
    }
}
