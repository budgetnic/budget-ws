package com.pernix.budget.ws.utils;

import com.pernix.budget.pojos.Damage;
import com.pernix.budget.pojos.Revision;
import com.pernix.budget.pojos.RevisionRequest;
import com.pernix.budget.pojos.Vehicle;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;

public class EnglishReportTranslation {

    private final static List<TreeMap<String, String>> treeMaps = getEnglishTranslations();

    private EnglishReportTranslation() {
        // utility class
    }

    public static void translateToEnglish(RevisionRequest revisionRequest, Vehicle vehicle) {
        
       
        translateGasToEnglish(vehicle);
    }

    public static String translateGas(String option){
        return treeMaps.get(FileName.GAS_LEVEL.getPosition()).get(option);
    }

    public static String translateParts(String option) {
        return treeMaps.get(FileName.PARTS.getPosition()).get(option);
    }

    public static String translateDamages(String option) {
        return treeMaps.get(FileName.DAMAGES.getPosition()).get(option);
    }

    public static String translateService(String option) {
        return treeMaps.get(FileName.SEVERITIES.getPosition()).get(option);
    }

    private static void translateGasToEnglish(Vehicle vehicle) {
        vehicle.setGas(treeMaps.get(FileName.GAS.getPosition()).get(vehicle.getGas()));
    }

    public static String translatePartOptionsToEnglish(String option) {
        return treeMaps.get(FileName.PART_OPTIONS.getPosition()).get(option);
    }

    public static String translatePartOptionsToSpanish(String option) {
        return treeMaps.get(FileName.PART_OPTIONSPA.getPosition()).get(option);
    }

    private static List<TreeMap<String, String>> getEnglishTranslations() {
        List<TreeMap<String, String>> treeMaps = new ArrayList<>();
        for(FileName fileName : FileName.values()) {
            treeMaps.add(TranslationData.loadData(fileName.getFileName()));
        }
        return treeMaps;
    }

    enum FileName {
        DAMAGES("damages", 0),
        PARTS("parts", 1),
        SEVERITIES("severities", 2),
        GAS_LEVEL("gasLevels", 3),
        GAS("gas", 4),
        PART_OPTIONS("partOptions", 5),
        PART_OPTIONSPA("partOptionSpanish", 6);

        private String fileName;
        private int position;

        FileName(String fileName, int position) {
            this.fileName = fileName;
            this.position = position;
        }

        public String getFileName() {
            return fileName;
        }

        public int getPosition() {
            return position;
        }
    }
}
