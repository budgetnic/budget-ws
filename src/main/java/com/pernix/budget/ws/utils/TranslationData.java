package com.pernix.budget.ws.utils;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.TreeMap;
 
public class TranslationData {

  private TranslationData() {
    // utility class
  }

  public static TreeMap<String, String> loadData(String fileName) {
    TreeMap<String, String> result = new TreeMap<>();
    try {
      FileReader fr = new FileReader("src/main/resources/translations/" + fileName + ".txt");
      BufferedReader br = new BufferedReader(fr);

      String line;
      String[] wordList;
      while ((line = br.readLine()) != null) {
        wordList = line.split("\\|");
        result.put(wordList[0], wordList[1]);
      }
      fr.close();
    } catch (Exception e) {
      e.printStackTrace();
    }
    return result;
  }
}