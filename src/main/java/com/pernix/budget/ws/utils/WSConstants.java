package com.pernix.budget.ws.utils;

public class WSConstants {

    public static final String REVISIONS_WS = "revisions";
    public static final String PDF_WS = "api/v1/pdf";
    public static final String STATUS_OK = "Ok";
    public static final String STATUS_FAILED = "Failed";
    public static final String VERSION = "api/v1/";

}
