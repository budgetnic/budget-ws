package com.pernix.budget.ws;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.pernix.budget.pojos.Damage;
import com.pernix.budget.pojos.Revision;
import com.pernix.budget.pojos.RevisionRequest;
import com.pernix.budget.ws.utils.TranslationData;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.TreeMap;

import javax.ws.rs.core.Response;

import org.junit.Test;

import de.svenjacobs.loremipsum.LoremIpsum;

public class PdfReportTest {

    @Test
    public void createPdf() throws Exception {
        RevisionRequest revisionRequest = mockRevisionRequest();
        PdfReport pdfReport = new PdfReport();
        Response response = pdfReport.createPdf(revisionRequest);
        saveToFile((byte[]) response.getEntity());
    }

    private RevisionRequest mockRevisionRequest() {
        RevisionRequest revisionRequest = new RevisionRequest();
        revisionRequest.setDamages(getDamages());
        revisionRequest.setObservations(mockObservations());
        revisionRequest.setRevision(loadFromJson());
        revisionRequest.setEmail("baguilar@pernixlabs.com");
        revisionRequest.setDeliveryPlaceMail("baguilar@pernixlabs.com");
        revisionRequest.setVehicleType("TRUCK");
        revisionRequest.setLanguage("english");
        return revisionRequest;
    }

    private Damage[] getDamages() {
        List<Damage> damages = new ArrayList<>();
        TreeMap<String, String> damagesMap = TranslationData.loadData("damages");
        Iterator<String> parts = TranslationData.loadData("parts").keySet().iterator();
        ListIterator<String> severities = new ArrayList<>(TranslationData.loadData("severities").keySet()).listIterator();
        for(Map.Entry<String, String> entry : damagesMap.entrySet()) {
            Damage damage = new Damage();
            damage.setDamage(entry.getKey());
            damage.setPart(parts.next());
            damage.setSeverity(severities.hasNext() ? severities.next() : severities.previous());
            damages.add(damage);
        }
        return damages.toArray(new Damage[0]);
    }

    private String[] mockObservations() {
        LoremIpsum loremIpsum = new LoremIpsum();
        return new String[] {loremIpsum.getWords(40), loremIpsum.getWords(50)};
    }

    private Revision loadFromJson() {
        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.readValue(
                    new File(System.getProperty("user.dir") + "/src/test/resources/revision.json"), Revision.class);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    private void saveToFile(byte[] byteArray) throws Exception {
        try(FileOutputStream fileOutputStream = new FileOutputStream("src/test/testReport.pdf")) {
            fileOutputStream.write(byteArray);
        }
    }
}