package com.pernix.budget.ws.utils;

import java.util.Map;
import java.util.TreeMap;

import org.junit.Test;

public class TranslationDataTest {

    @Test
    public void loadParts() {
        TreeMap<String, String> treeMap = TranslationData.loadData("parts");
        assert(treeMap.size() > 0);
        printTreeMap(treeMap);
    }

    @Test
    public void loadDamages() {
        TreeMap<String, String> treeMap = TranslationData.loadData("damages");
        assert(treeMap.size() > 0);
        printTreeMap(treeMap);
    }

    @Test
    public void loadSeverities() {
        TreeMap<String, String> treeMap = TranslationData.loadData("severities");
        assert(treeMap.size() > 0);
        printTreeMap(treeMap);
    }

    private void printTreeMap(TreeMap<String, String> treeMap) {
        for(Map.Entry<String, String> entry : treeMap.entrySet()) {
            String key = entry.getKey();
            String value = entry.getValue();
            System.out.println(key + " => " + value);
        }
    }
}